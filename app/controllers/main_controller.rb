require 'rinruby'

class MainController < ApplicationController

  @@r=RinRuby.new(:echo=>false)
  @@categories = [
    ["ALIMENTARY TRACT AND METABOLISM", :A],
    ["BLOOD AND BLOOD FORMING ORGANS", :B],
    ["CARDIOVASCULAR SYSTEM", :C],
    ["DERMATOLOGICALS", :D],
    ["GENITO URINARY SYSTEM AND SEX HORMONES", :G],
    ["SYSTEMIC HORMONAL PREPARATIONS, EXCL. SEX HORMONES AND INSULINS", :H],
    ["ANTIINFECTIVES FOR SYSTEMIC USE", :J],
    ["ANTINEOPLASTIC AND IMMUNOMODULATING AGENTS", :L],
    ["MUSCULO-SKELETAL SYSTEM", :M],
    ["NERVOUS SYSTEM", :N],
    ["ANTIPARASITIC PRODUCTS, INSECTICIDES AND REPELLENTS", :P],
    ["RESPIRATORY SYSTEM", :R],
    ["SENSORY ORGANS", :S],
    ["VARIOUS", :V]
  ]

  def binning
    @show_pictures = false
    set_binning
  end

  def binning_generate
    @show_pictures = true
    set_binning
    @@r.eval "setwd('#{Dir.pwd}')"
    @@r.bins = @bins.to_i
    @@r.lag = @lag.to_i
    @@r.param = (@weather_param.split(' ')).join('.')
    @@r.medicine_group = @medicine_group
    @@r.time_unit = @time_unit
    file = (params[:binning] == 'equal-freq' ? 'R/equal-freq.R' : 'R/equal-width.R')
    @@r.eval File.read(file)
    render 'binning'
  end

  def clustering
    @show_pictures = false
    set_clustering
  end

  def clustering_generate
    set_clustering
    flash[:message] = nil
    @show_pictures = true
    if (@weather_param.blank? || @weather_param.empty?)
      @show_pictures = false
      flash[:message] = "Choose weather parameter"
    else
      @@r.eval "setwd('#{Dir.pwd}')"
      @@r.k = @clusters.to_i
      @@r.alpha = @alpha
      @@r.minPts = @minPts
      @@r.time_unit = @time_unit
      @@r.clusteringColumns = @weather_param.map{|p, v| p}
      @@r.medicine_group = @medicine_group
      @@r.clustering_name = @clustering
      @@r.eval File.read('R/Project.R')
    end
    render 'clustering'
  end

  def about
  end

  def algorithms
  end

private
  def set_clustering
    session['weather_param'] = @weather_param = params['weather_param']
    session['clusters'] = @clusters = params['clusters'] || 4
    session['medicine_group'] = @medicine_group = params['medicine_group'] || :A
    session['clustering'] = @clustering = params['clustering'] || "kmeans"
    session['time_unit'] = @time_unit = params['time_unit'] || "week"
    session['alpha'] = @alpha = params['alpha'] || 0.2
    session['minPts'] = @minPts = params['minPts'] || 5
    @medicine_family = []
    @medicine_family << File.read("data/medicine_family1.txt").split("\n").map{|record| record.split("\t")}
    @medicine_family << File.read("data/medicine_family2.txt").split("\n").map{|record| record.split("\t")}
    @medicine_family << File.read("data/medicine_family3.txt").split("\n").map{|record| record.split("\t")}
    @medicine_family << File.read("data/medicine_family4.txt").split("\n").map{|record| record.split("\t")}
    @categories = @@categories
  end

  def set_binning
    session['weather_param'] = @weather_param = params['weather_param'] || "Temperature"
    session['bins'] = @bins = params['bins'] || 4
    session['lag'] = @lag = params['lag'] || 1
    session['time_unit'] = @time_unit = params['time_unit'] || "week"
    session['binning'] = @binning = params['binning'] || "equal-freq"
    session['medicine_group'] = @medicine_group = params['medicine_group'] || :A
    @medicine_family = []
    @medicine_family << File.read("data/medicine_family1.txt").split("\n").map{|record| record.split("\t")}
    @medicine_family << File.read("data/medicine_family2.txt").split("\n").map{|record| record.split("\t")}
    @medicine_family << File.read("data/medicine_family3.txt").split("\n").map{|record| record.split("\t")}
    @medicine_family << File.read("data/medicine_family4.txt").split("\n").map{|record| record.split("\t")}
    @categories = @@categories
  end

end
